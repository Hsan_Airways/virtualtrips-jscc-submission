import * as bodyParser from 'body-parser'

import LocationRouter from './routers/location-router'
import cors from 'cors'
import express from 'express'

const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

app.use('/locations', LocationRouter)

export default app
