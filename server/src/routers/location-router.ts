import { LocationController } from '../controller/LocationController'
import { Router } from 'express'
import { query } from 'express-validator'

const router: Router = Router()

const locationController = new LocationController()

router.get('/', query('q').isString(), locationController.search)

export default router
