import { MigrationInterface, QueryRunner } from 'typeorm'

import { AppDataSource } from '../data-source'
import { Location } from '../entity/Location'
import { createReadStream } from 'fs'
import { parse } from 'csv-parse'

export class seedLocations1658856578900 implements MigrationInterface {
	private async loadLocations(): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			let totalRows: number = 0
			createReadStream(__dirname + '/../rawData/GB.tsv')
				.pipe(
					parse({
						delimiter: '\t',
						from_line: 2,
						relax_column_count: true,
						relax_quotes: true,
					})
				)
				.on('data', async (row) => {
					totalRows++

					await AppDataSource.createQueryBuilder()
						.insert()
						.into(Location)
						.values({
							geonameid: parseInt(row[0]),
							name: row[1],
							asciiname: row[2],
							alternatenames: row[3],
							latitude: parseFloat(row[4]),
							longitude: parseFloat(row[5]),
							feature_class: row[6],
							feature_code: row[7],
							country_code: row[8],
							cc2: row[9],
							admin1_code: row[10],
							admin2_code: row[11],
							admin3_code: row[12],
							admin4_code: row[13],
							population: row[14],
							elevation: row[15],
							dem: parseInt(row[16]),
							timezone: row[17],
							modification_date: row[18],
						})
						.execute()
				})
				.on('end', () => {
					console.log('SEED ENDED')
					console.log('TOTAL ROWS:' + totalRows)
					resolve()
				})
		})
	}

	public async up(queryRunner: QueryRunner): Promise<void> {
		await this.loadLocations()
	}

	public async down(queryRunner: QueryRunner): Promise<void> {}
}
