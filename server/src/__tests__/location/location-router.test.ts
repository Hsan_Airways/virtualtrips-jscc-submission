import { AppDataSource } from '../../data-source'
import app from '../../app'
import request from 'supertest'

beforeAll(() => {
	AppDataSource.initialize()
})

describe('GET /locations', () => {
	it('it returns status 400 if no query is provided', async () => {
		const res = await request(app).get('/locations')
		expect(res.statusCode).toEqual(400)
	})

	it('it returns status 200 and empty result set if the query is empty', async () => {
		const res = await request(app).get('/locations?q=')
		expect(res.statusCode).toEqual(200)
		expect(res.type).toEqual('application/json')
		expect(res.body).toEqual([])
	})

	it('it returns status 200 and empty result set if the query is one char long', async () => {
		const res = await request(app).get('/locations?q=x')
		expect(res.statusCode).toEqual(200)
		expect(res.type).toEqual('application/json')
		expect(res.body).toEqual([])
	})

	it('it returns status 200 and and a result set if query is 2 chars or more', async () => {
		const res = await request(app).get('/locations?q=test')
		expect(res.statusCode).toEqual(200)
		expect(res.type).toEqual('application/json')
		expect(res.body).toEqual(['Teston', 'Test Valley District'])
	})
})
