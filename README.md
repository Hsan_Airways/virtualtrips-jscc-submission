
# Virtualtrips JavaScript code challenge submission

## How to run server

1. cd into the server directory
3. `npm install`
3. Create an env file .env with the content of `PORT=3001`
4. `npm run dev`
5. open a new termianl window and `npm run migration:run`
4. To run the tests `npm run test`

## How to run the client

1. cd into the client directory
2. Create an env file .env with the content of `REACT_APP_BASE_URL=3001`
3. `npm install`
4. `npm start`
4. To run the tests `npm run test`