import { render, screen, waitFor } from '@testing-library/react'

import App from './App'
import userEvent from '@testing-library/user-event'

describe('Search Locations', () => {
	it('renders a search box', () => {
		render(<App />)
		const inputElement = screen.getByPlaceholderText('Start typing')
		expect(inputElement).toBeInTheDocument()
	})

	it('updates on change', async () => {
		render(<App />)
		const inputElement = screen.getByPlaceholderText('Start typing')
		await userEvent.type(inputElement, 'test')
		expect(inputElement).toHaveValue('test')
	})

	it('show no results when user types one character', async () => {
		render(<App />)
		const inputElement = screen.getByPlaceholderText('Start typing')
		await userEvent.type(inputElement, 'x')
		const resultsList = screen.getByTestId('results')
		expect(resultsList.childNodes.length).toEqual(0)
	})

	it('shows results when user types more than two chars', async () => {
		render(<App />)
		const inputElement = screen.getByPlaceholderText('Start typing')
		await userEvent.type(inputElement, 'test')
		const resultsList = screen.getByTestId('results')
		await waitFor(() => expect(resultsList.childNodes.length).toBeGreaterThan(0))
	})
})
