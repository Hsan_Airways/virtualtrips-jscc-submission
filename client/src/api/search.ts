import api from './base'

export const searchLocations = async (query: string): Promise<string[]> => {
	const res = await api.get(`/locations?q=${query}`)

	return res.data
}
